<?php

abstract class AbstractDeveloper
{
    abstract public function getProjectA(): ProjectA;

    abstract public function getProjectB(): ProjectB;
}


class FirstDeveloper extends AbstractDeveloper
{

    public function getProjectA(): PatternsA
    {
        return new PatternsA();
    }

    public function getProjectB(): PatternsB
    {
        return new PatternsB();
    }
}


class SecondDeveloper extends AbstractDeveloper
{

    public function getProjectA(): TgBotA
    {
        return new TgBotA();
    }

    public function getProjectB(): TgBotB
    {
        return new TgBotB();
    }
}


abstract class ProjectA
{
    /**
     * @return string
     */
    public abstract function getProjectName(): string;
}

class  PatternsA extends ProjectA
{
    public function __construct()
    {
        var_dump("PatternsA");
    }

    public function getProjectName(): string
    {
        return 'Project first developer - PatternsA';
    }
}

class TgBotA extends ProjectA
{
    public function __construct()
    {
        var_dump("TgBotA");
    }

    public function getProjectName(): string
    {
        return 'Project second developer - TgBotA';
    }
}


abstract class ProjectB
{
    /**
     * @return string
     */
    public abstract function getProjectName(): string;
}

class  PatternsB extends ProjectB
{
    public function __construct()
    {
        var_dump("PatternsB");
    }

    public function getProjectName(): string
    {
        return 'Project first developer - PatternsB';
    }
}

class TgBotB extends ProjectB
{
    public function __construct()
    {
        var_dump("TgBotB");
    }

    public function getProjectName(): string
    {
        return 'Project second developer - TgBotB';
    }
}



if (isset($argv[1])) {
    switch ($argv[1]) {
        case 1:
            $firstDeveloper = new FirstDeveloper();
            $firstDeveloper->getProjectA();
            $firstDeveloper->getProjectB();
            break;
        case 2:
            $secondDeveloper = new SecondDeveloper();
            $secondDeveloper->getProjectA();
            $secondDeveloper->getProjectB();
            break;

        default:
            echo "Not fount type";
    }
}