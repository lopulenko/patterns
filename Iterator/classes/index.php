<?php
include 'TasksCollection.php';


$collection = new TasksCollection();
$collection->addItem("First");
$collection->addItem("Second");
$collection->addItem("Third");

echo "Straight traversal:\n";
try {
    foreach ($collection->getIterator() as $item) {
        echo $item . "\n";
    }
} catch (Exception $e) {
}

echo "\n";
echo "Reverse traversal:\n";
foreach ($collection->getReverseIterator() as $item) {
    echo $item . "\n";
}