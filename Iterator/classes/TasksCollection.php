<?php
include 'AlphabeticalTaskIterator.php';

class TasksCollection implements \IteratorAggregate
{
    private array $items = [];

    public function getIterator(): AlphabeticalTaskIterator
    {
        return new AlphabeticalTaskIterator($this);
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem($item)
    {
        $this->items[] = $item;
    }

    public function getReverseIterator(): AlphabeticalTaskIterator
    {
        return new AlphabeticalTaskIterator($this, true);
    }
}