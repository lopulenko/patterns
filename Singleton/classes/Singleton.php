<?php

namespace singleton\classes;

class Singleton
{
    private static $instance = null;

    private function __clone()
    {
        //TODO
    }

    private function __construct()
    {
    }


    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function run($text)
    {
        var_dump($text);
    }

}

$object1 = Singleton::getInstance();
$object1->run('test');

//$object2 = new Singleton();
//$object3 = clone $object1;