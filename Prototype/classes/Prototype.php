<?php

class Human
{
    public string $name;

    public function __clone()
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name ?: 'HumanName';
    }
}


class PrototypeHuman
{
    public function getClone(Human $human): Human
    {
        return clone $human;
    }
}

$human = new Human();
$human->setName('Robert');
$prototype = new PrototypeHuman();

$cloneHuman = $prototype->getClone($human);
var_dump($cloneHuman->getName());