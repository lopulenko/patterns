<?php

include '../../Singleton/classes/BaseClass.php';


use singleton\classes\BaseClass;

class Prototype
{
    public function getClone(BaseClass $single)
    {
        return clone $single;
    }
}

$prototype = new Prototype();
$single = $prototype->getClone(new BaseClass());