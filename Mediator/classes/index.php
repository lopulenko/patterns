<?php
include 'Developer1.php';
include 'Developer2.php';
include 'Manager.php';

$c1 = new Developer1();
$c2 = new Developer2();
$mediator = new Manager($c1, $c2);

echo "Client triggers project A.\n";
$c1->doA();

echo "\n";
echo "Client triggers project D.\n";
$c2->doD();