<?php

class Developer2 extends BaseDeveloper
{
    public function doC(): void
    {
        echo "Developer2 does C.\n";
        $this->mediator->notify($this, "C");
    }

    public function doD(): void
    {
        echo "Developer2 does D.\n";
        $this->mediator->notify($this, "D");
    }
}