<?php
include 'BaseDeveloper.php';

class Developer1 extends BaseDeveloper
{
    public function doA(): void
    {
        echo "Developer1 does A.\n";
        $this->mediator->notify($this, "A");
    }

    public function doB(): void
    {
        echo "Developer1 does B.\n";
        $this->mediator->notify($this, "B");
    }
}