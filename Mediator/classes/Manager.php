<?php
include 'Mediator.php';

class Manager implements Mediator
{
    private Developer1 $developer1;

    private Developer2 $developer2;

    public function notify(object $sender, string $event): void
    {
        if ($event == "A") {
            echo "Manager reacts on A and triggers following project:\n";
            $this->developer2->doC();
        }

        if ($event == "D") {
            echo "Manager reacts on D and triggers following project:\n";
            $this->developer1->doB();
            $this->developer2->doC();
        }
    }

    public function __construct(Developer1 $d1, Developer2 $d2)
    {
        $this->developer1 = $d1;
        $this->developer1->setMediator($this);
        $this->developer2 = $d2;
        $this->developer2->setMediator($this);
    }

}
