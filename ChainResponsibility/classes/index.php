<?php

include 'JuniorDeveloper.php';
include 'MiddleDeveloper.php';
include 'SeniorDeveloper.php';


function run(Handler $handler)
{
    $arrProjectLevels = ['Junior', 'Middle', 'Senior'];

    foreach ($arrProjectLevels as $projectLevel) {
        echo "Manager: Who can complete a level project " . $projectLevel . "?\n";
        $result = $handler->handle($projectLevel);
        if ($result) {
            echo "  " . $result;
        } else {
            echo "  " . $projectLevel . " remained unfulfilled.\n";
        }
    }
}



$junior = new JuniorDeveloper();
$middle = new MiddleDeveloper();
$senior = new SeniorDeveloper();


$junior->setNext($middle)->setNext($senior);


echo "Chain: Junior > Middle > Senior\n\n";
run($junior);
echo "\n\n";

echo "SubChain: Middle > Senior\n\n";
run($middle);
echo "\n\n";

echo "SubChain: Senior\n\n";
run($senior);