<?php

class SeniorDeveloper extends \AbstractHandler
{
    public function handle(string $request): ?string
    {
        if ($request === "Senior") {
            return "SeniorDeveloper: i can do a " . $request . " project.\n";
        } else {
            return parent::handle($request);
        }
    }
}