<?php

class MiddleDeveloper extends \AbstractHandler
{
    public function handle(string $request): ?string
    {
        if ($request === "Middle") {
            return "MiddleDeveloper: i can do a " . $request . " project.\n";
        } else {
            return parent::handle($request);
        }
    }
}