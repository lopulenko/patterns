<?php
include 'AbstractHandler.php';

class JuniorDeveloper extends \AbstractHandler
{
    public function handle(string $request): ?string
    {
        if ($request === "Junior") {
            return "JuniorDeveloper: i can do a " . $request . " project.\n";
        } else {
            return parent::handle($request);
        }
    }
}