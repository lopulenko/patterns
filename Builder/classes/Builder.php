<?php

class Project
{
    private array $modules;

    public function add($element)
    {
        $this->modules[] = $element;
    }

    public function getProject()
    {
        foreach ($this->modules as $module) {
            echo $module . '<br>';
        }
    }
}

class Manager
{
    public function setConstruct($developer)
    {
        $developer->createBackend();
        $developer->createFrontend();
    }
}


abstract class DeveloperAbstract
{
    public function createBackend()
    {
    }

    public function createFrontend()
    {
    }

    abstract public function getResult();
}


class MiddleDeveloper extends DeveloperAbstract
{
    private Project $project;

    public function __construct()
    {
        $this->project = new Project();
    }

    public function createBackend()
    {
        $this->project->add('MiddleDeveloper add back module');
    }

    public function createFrontend()
    {
        $this->project->add('MiddleDeveloper add front module');
    }

    public function getResult(): Project
    {
        return $this->project;
    }
}



class JuniorDeveloper extends DeveloperAbstract
{
    private Project $project;

    public function __construct()
    {
        $this->project = new Project();
    }

    public function createBackend()
    {
        $this->project->add('JuniorDeveloper add back module');
    }

    public function createFrontend()
    {
        $this->project->add('JuniorDeveloper add front module');
    }

    public function getResult(): Project
    {
        return $this->project;
    }
}




$manager = new Manager();

$juniorDeveloper = new JuniorDeveloper();
$middleDeveloper = new MiddleDeveloper();

$manager->setConstruct($juniorDeveloper);
$project1 = $juniorDeveloper->getResult();
$project1->getProject();

$manager->setConstruct($middleDeveloper);
$project2 = $middleDeveloper->getResult();
$project2->getProject();