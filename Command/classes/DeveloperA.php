<?php

class DeveloperA
{
    public function doSomething(string $a): void
    {
        echo "DeveloperA: Working on (" . $a . ".)\n";
    }

    public function doSomethingElse(string $b): void
    {
        echo "DeveloperA: Also working on (" . $b . ".)\n";
    }
}