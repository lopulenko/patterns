<?php

class ComplexCommand implements Command
{
    /**
     * @var DeveloperA
     */
    private DeveloperA $developer;

    /**
     * Данные, необходимые для запуска методов.
     */
    private string $a;

    private string $b;

    /**
     * Сложные команды могут принимать один или несколько объектов-получателей
     * вместе с любыми данными о контексте через конструктор.
     */
    public function __construct(DeveloperA $developer, string $a, string $b)
    {
        $this->developer = $developer;
        $this->a = $a;
        $this->b = $b;
    }

    /**
     * Команды могут делегировать выполнение любым методам получателя.
     */
    public function execute(): void
    {
        echo "ComplexCommand: Complex stuff should be done by a receiver object.\n";
        $this->developer->doSomething($this->a);
        $this->developer->doSomethingElse($this->b);
    }
}