<?php
include 'ManagerA.php';
include 'DeveloperA.php';
include 'SimpleCommand.php';
include 'ComplexCommand.php';

$managerA = new ManagerA();
$managerA->setOnStart(new SimpleCommand("Say Hi!"));
$developerA = new DeveloperA();
$managerA->setOnFinish(new ComplexCommand($developerA, "Send email", "Save report"));

$managerA->doSomethingImportant();