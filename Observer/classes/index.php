<?php
include 'SteamPlayer.php';
include 'FriendFirst.php';
include 'FriendSecond.php';


try {
    $subject = new SteamPlayer();

    $f1 = new FriendFirst();
    $subject->attach($f1);

    $f2 = new FriendSecond();
    $subject->attach($f2);

    $subject->play();
    $subject->play();

    $subject->detach($f2);

    $subject->play();
} catch (Exception $e) {
    echo $e->getMessage();
}