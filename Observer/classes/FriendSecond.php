<?php

class FriendSecond implements \SplObserver
{
    public function update(\SplSubject $subject): void
    {
        if ($subject->state == 2) {
            echo "FriendSecond: наблюдает как за игрой в Neverwinter.\n";
        }
    }
}