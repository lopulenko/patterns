<?php

class FriendFirst implements \SplObserver
{
    public function update(\SplSubject $subject): void
    {
        if ($subject->state == 1) {
            echo "FriendFirst: наблюдает как за игрой в CsGo.\n";
        }
    }
}