<?php

class SteamPlayer implements SplSubject
{
    /**
     * @var int В этой переменной хранится состояние игрока.
     */
    public int $state;

    /**
     * @var SplObjectStorage Список наблюдателей.
     */
    private SplObjectStorage $observers;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    /**
     * Методы управления подпиской.
     */
    public function attach(\SplObserver $observer): void
    {
        echo "Subject: прикрепил наблюдателя.\n";
        $this->observers->attach($observer);
    }

    public function detach(\SplObserver $observer): void
    {
        $this->observers->detach($observer);
        echo "Subject: отключил наблюдателей.\n";
    }

    /**
     * Запуск обновления в каждом подписчике.
     */
    public function notify(): void
    {
        echo "Subject: уведомил друзей...\n";
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }


    public function play(): void
    {
        echo "\nSubject: я запустил стрим.\n";
        $this->state = rand(1, 2);

        $game = $this->state == 1 ? "CsGo" : "Neverwinter";


        echo "Subject: запустил игру: {$game}\n";
        $this->notify();
    }
}