<?php

namespace classes\factoryMethod;

abstract class Human
{
    public abstract function sayHello($text);
}

class Man extends Human
{
    public function sayHello($text): void
    {
        var_dump('Man: Hello World! ' . $text);
    }
}


class Woman extends Human
{
    public function sayHello($text): void
    {
        var_dump('Woman: Hello World! ' . $text);
    }
}

class Factory
{
    public function create(string $className = 'Man'): Human
    {
        switch ($className) {
            case'Man':
                return new Man();
            case'Woman':
            default:
                return new Woman();
        }
    }
}


$className = 'Man';
$factory = new Factory();
$object = $factory->create($className);

$object->sayHello('I\'m Human');



